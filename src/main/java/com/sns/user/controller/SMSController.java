package com.sns.user.controller;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.sns.user.model.AjaxResponseBody;
import com.sns.user.model.PrintMessage;
import com.sns.user.model.SMSModel;
import com.sns.user.repository.GroupRepository;
import com.sns.user.service.SMSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;



@RestController
@CrossOrigin
public class SMSController {

    @Autowired
    SMSService service;
    @Autowired
    GroupRepository groupRepository;

    @PostMapping(value = "/api/send")
    public ResponseEntity<AjaxResponseBody> getSearchResultViaAjax(@RequestBody PrintMessage print, Errors errors){
        AjaxResponseBody result = new AjaxResponseBody();

        if (errors.hasErrors()) {
            result.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            return ResponseEntity.badRequest().body(result);

        }
        SMSModel smsModel = new SMSModel("AKIAI7OWFTPYVTW3ERGA","SsUw/mvqcaaimscO6x9glE9rHfBiVMwNYHVATIgr");
        String ACCESS_KEY = smsModel.getACCESS_KEY();
        String SECRET_KEY = smsModel.getSECRET_KEY();
        String topicName = "SMS_GROUP";

        List<String> phoneNumbers = groupRepository.retrievePhoneNumbers();
        AmazonSNSClient snsClient = new AmazonSNSClient(new BasicAWSCredentials(ACCESS_KEY,SECRET_KEY));
        String topicArn = service.createSNSTopic(snsClient, topicName);
        service.subscribeToTopic(snsClient,topicArn,"sms",phoneNumbers);
        String sms = service.sendSMSMessageToTopic(snsClient, topicArn, print.getMessage());
        if (sms == null) {
            result.setMsg("Message not delivered");
        } else {
            result.setMsg("Message delivered");
        }
        result.setResult(sms);

        return ResponseEntity.ok(result);

    }

}

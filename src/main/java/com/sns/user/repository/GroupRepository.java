package com.sns.user.repository;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Repository

public class GroupRepository {

    private List<String> phoneNumbers;
    @PostConstruct
    public void addPhoneNumbers(){
        phoneNumbers = new ArrayList<>();
        phoneNumbers.add("+12149294443");
        phoneNumbers.add("+16195666358");
    }
    public List<String> retrievePhoneNumbers(){
        return phoneNumbers;
    }

}

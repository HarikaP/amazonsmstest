package com.sns.user.model;

public class PrintMessage {
    private String message;
    public PrintMessage(){

    }

    public PrintMessage(String message){

        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

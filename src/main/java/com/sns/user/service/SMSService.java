package com.sns.user.service;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.*;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class SMSService {


    public static int count =0;

    public String createSNSTopic(AmazonSNSClient snsClient, String topicName){
        CreateTopicRequest createTopic = new CreateTopicRequest(topicName);
        CreateTopicResult result = snsClient.createTopic(createTopic);
        return result.getTopicArn();
    }
    public void subscribeToTopic(AmazonSNSClient snsClient,String topicArn, String protocol, List<String> phoneNumbers){
        for(String phoneNumber:phoneNumbers){
            SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn,protocol,phoneNumber);
            snsClient.subscribe(subscribeRequest);
            count++;
        }
    }
    public String sendSMSMessageToTopic(AmazonSNSClient snsClient, String topicArn,String message){
        PublishResult result = snsClient.publish(new PublishRequest().withTopicArn(topicArn).withMessage(message));
        System.out.println(result.getMessageId());
        System.out.println("total number of messages sent:"+count);
        return result.getMessageId();
    }
}

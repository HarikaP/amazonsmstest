package com.sns.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SendSMS {
    public static void main(String[] args){
        SpringApplication.run(SendSMS.class,args);
    }
}

$(document).ready(function () {

    $("#sendMsg").submit(function (event) {
        event.preventDefault();
        fire_ajax_submit();
    });
});

function fire_ajax_submit() {

    var input = {}
    input["message"] = $("#msg").val();

    $("#sendMsg").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/api/send",
        data: JSON.stringify(input),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(input, null, 4) + "</pre>";
            //$('#feedback').html(json);
            $('#list').html(json);
            console.log("SUCCESS : ", data);
            $("#sendMsg").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            // $('#feedback').html(json);
            $('#list').html(json);

            console.log("ERROR : ", e);
            $("#sendMsg").prop("disabled", false);

        }
    });

}
